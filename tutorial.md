On this tutorial, we're going to build a twitter scraper. This scraper is going to scrape Twitter for tweets using the #MLHLocalhost.
Our app is going to be built using Node.js, express, and scrapeIt.

Let's start by setting up Node.js.

## Installing node
For you to install Node.js it's simple. All you have to do is access [node's download](https://nodejs.org/en/download/) page and download the right version for your OS and install it.

#### Windows

If you're using windows, download the `Windows Installer (.msi)`

#### Mac Os
If you're using mac os, download the `macOS Installer (.pkg)`

#### Ubuntu
If you're using Ubuntu, you can follow [this tutorial](https://www.digitalocean.com/community/tutorials/how-to-install-node-js-on-ubuntu-18-04) from Digital Ocean.

Now that Node.js is set up, it's time to start building the app.
The first step is to create a new node application. Let's do it:
  1. in a terminal run the command `npm  init`. When prompted, enter the application name as mlh-twitter-scraper, and then press return to accept the default for each following option. When you've accepted all the defaults, you'll see text printed to the console that looks something like this:
  ```
    {
      "name": "mlh-twitter-scraper",
      "version": "1.0.0",
      "description": "",
      "main": "index.js",
      "scripts": {
        "test": "echo \"Error: no test specified\" && exit 1"
      },
      "author": "",
      "license": "ISC",
    }
  ```
  2. let's install the dependencies `express`, `scrapeIt`, `sanitize-html`, `ejs` and `nodemon`
  ```
    npm install express scrapeIt sanitize-html ejs --save
  ```
  ```
    npm installl nodemon --save-dev
  ```
  3. change `scripts` in package.json to be like this:
  ```
    "scripts": {
      "start": "nodemon app/server.js"
    },
  ```
  Don't worry about `app/server.js` we're going to create it on further steps.

  4. Change `main` in package.json to be like this:
  ```
    "main": "app/server.js",
  ```

After you have finished all the steps, this is how your package.json should look like:
```
  {
      "name": "mlh-twitter-scraper",
      "version": "1.0.0",
      "description": "",
      "main": "app/server.js",
      "scripts": {
        "start": "nodemon app/server.js"
      },
      "author": "",
      "license": "ISC",
      "dependencies": {
        "ejs": "^2.6.1",
        "express": "^4.16.4",
        "sanitize-html": "^1.19.1",
        "scrape-it": "^5.1.0"
      },
      "devDependencies": {
        "nodemon": "^1.18.4"
      }
  }
```

So far we have Node.js set up, the project created and the dependencies installed.
Let's go ahead and create the folder structure that's going to be used:
  1. create an `app` folder
  2. inside `app` folder, create `assets`, `controllers`, `routes`, `services` and `views`.
Your folder structure should look like this:
```
mlh-twitter-scraper
| app
| | assets
| | controllers
| | routes
| | services
| | views
```

I know you're thinking `okay Diogo, I have Node.js installed, I have the project and folder structure created, but when am I going to start coding?` and I have good news: it's now.

Let's create the entry point file for our app: `app/server.js`. Inside this file let's add the following code:
```
const app = require('./app');

const port = process.env.PORT || 8080;

app.listen(port);
console.log(`Listening at http://localhost:${port}`);

```
This tells Node.js to start the server and listen on a specific port that will be defined by the ENV var PORT or by the default value 8080 used in case no PORT ENV var is defined.

As you can see, the server is using an object exported by `app/app.js` and without it, we cannot run the app. So create it and add the following code:
```
const express = require('express');

const routes = require('./routes');

const app = express();

app.use(routes);

module.exports = app;
```
On this file we let express know what the endpoints for our app are.
Using the `app.usage` function, it mounts the routes declared on `app/routes/index.js`.

Node.js already knows how to start the server, which port to listen and express knows the routes used by the app. Wait does express know what the routes are? Not really, we're still missing the `app/routes/index.js` file.
Let's work on this, create the file and add the following code on it:
```
1. const express = require('express');
2. const path = require('path');
3.
4. const Tweets = require('../controllers/tweets');
5.
6. const assetsPath = express.static(path.join(__dirname, '../assets'));
7. const viewsPath = path.join(__dirname, '../views');
8.
9. const client = express();
10.
11. client.set('views', viewsPath);
12. client.set('view engine', 'ejs');
13.
14. client.use(assetsPath);

15. client.get('/', Tweets.get);

16. module.exports = client;

```

let's talk about what is happening here.
To handle incoming requests express needs to know which function or functions to execute given a path. For this, we declare the routes using the format `client.METHOD(path, handler)` where
`client` is an express instance, `METHOD` is one of the HTTP methods, in lower case, `path` is the endpoint and `handler` is the function that is going to handle the request.

So, on line 15 we're telling to express `hey express, when you get a GET request to the / path, please execute Tweets.get function"`
Moreover, on this file, we're setting up the assets and views paths and also the view engine.

`EJS` is the templating engine we're going to use on our app.

```
EJS is a templating language that lets you generate HTML markup. You can check more about it [here](http://ejs.co/#install).
```

It's time to build the `Tweets` controller.
  1. create a folder called `tweets` in `controllers` folder
  2. create an index.js in `tweets` folder and add the following code:
  ```
    const { ScraperService } = require('../../services');

    const get = (req, res) => {
      ScraperService.scrape('mlhLocalhost').then(({ data }) => {
        const tweets = data.tweets.filter(tweet => tweet.content && tweet.content.length > 0);
        res.render('tweets', { tweets });
      });
    };

    module.exports = {
      get
    };
  ```

On this controller, we're doing a pretty simple operation: ask ScraperService to scrape tweets with the hashtag `mlhLocalhost` and render them on the tweets page.

Speaking of the ScrapeService, let's work on it:
  1. create a folder called `scraper` in `services` folder
  2. create an `index.js` file inside `scraper` and paste the following code:
  ```
    const sanitizeHtml = require('sanitize-html');
    const scrapeIt = require('scrape-it');

    const scrape = (hastag) => {
      const options = {
        tweets: {
          listItem: '.stream-item',
          data: {
            avatar: {
              selector: '.avatar',
              attr: 'src',
            },
            name: '.fullname',
            content: {
              convert: (data) => data && sanitizeHtml(data, {
                allowedTags: [ 'b', 's', 'strong' ],
              }),
              selector: '.tweet-text',
              how: 'html'
            },
            userName: '.stream-item-header .username b',
          }
        }
      };

      return scrapeIt(`https://twitter.com/search?q=%23${hastag}`, options);
    };

    module.exports = {
      scrape,
    };
  ```
  3. now create an `index.js` file in `services` folder and add the following:
  ```
    const ScraperService = require('./scraper');

    module.exports = {
      ScraperService,
    };
  ```

`services/index.js` is where we're going to export all the services we have.
`services/scraper/index.js` is where we'll add the code responsible for scraping Twitter for tweets using the #mlhLocalhost.
To scrape the tweets, we're going to use the [scapeIt](https://github.com/IonicaBizau/scrape-it) library that allows us to, given an URL, reach out the desired web page, get its content and parse it into a JSON.
In our app we're going to read `https://twitter.com/search?q=#mlhLocalhost`.
The first step is to let scrapeIt knows how we want the data to be parsed. For that, we built the object `options` that has the configuration for scrapeIt. After that, we can execute the `scrapeIt()` function passing as parameters the url and the options that will be used to build the JSON.
That's it, our service to get the tweets is done.

We're almost done with our app. It's only missing the front end, so let's build the page to render the tweets.

First step is to create the file `tweets.ejs` inside `views` folder. After you create the file, paste the following code:
```
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Add the slick-theme.css if you want default styling -->
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.1/slick/slick.css"/>
    <link rel="icon" type="image/png" href="images/favicon.ico" />
    <title>Major League Hacking (MLH)</title>
    <link rel="stylesheet" href="styles/index.css">
</head>
<body class="mdl-demo mdl-color--grey-100 mdl-color-text--grey-700 mdl-base">
  <div class="logo-container">
    <img src="images/mlh-localhost-logo-dark.svg" />
  </div>
  <div class="tweets-container">
    <div class="tweets">
      <% for(tweet of tweets) {%>
        <div class="tweet">
          <div class="avatar">
            <img src="<%= tweet.avatar %>" />
          </div>
          <div>
            <span class="username">
              <%= tweet.name %>
              <a href="https://twitter.com/<%= tweet.userName %>">@<%= tweet.userName %></a>
            </span>
            <div>
              <%- tweet.content %>
            </div>
          </div>
        </div>
        <% } %>
      </div>
  </div>
  <script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>
  <script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
  <script type="text/javascript" src="//cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.1/slick/slick.min.js"></script>
  <script type="text/javascript" src="scripts/index.js"></script>
</body>
</html>
```

The special code on this page is here:
```
<% for(tweet of tweets) {%>
  <div class="tweet">
    <div class="avatar">
      <img src="<%= tweet.avatar %>" />
    </div>
    <div>
      <span class="username">
        <%= tweet.name %>
        <a href="https://twitter.com/<%= tweet.userName %>">@<%= tweet.userName %></a>
      </span>
      <div>
        <%- tweet.content %>
      </div>
    </div>
  </div>
<% } %>
```

This is the code responsible for getting the `tweets` parameter that `Tweets.get` endpoint returned, walk through them and render each tweet individually. It's possible because of EJS.
Using EJS syntax, we built a for loop
```
<% for(tweet of tweets) {%>
...
<% } %>
```
and printed values:
```
<%= tweet.avatar %>
```

Besides that we have some css and js utilities code that I'm pasting below, but feel free to change them and play with it.

`app/assets/styles/index.css`
```
body {
  display: flex;
  flex-direction: column;
  height: 100vh;
  margin: 0;
  overflow: hidden;
}

.logo-container {
  display: flex;
  justify-content: center;
  padding: 20px;
}

.tweets-container {
  background: #f8f8f8;
  flex: 1;
  padding: 100px;
}

.tweets {
  display: none;
  margin: 0 auto;
  width: 50vw;
}

.slick-vertical .tweet.slick-slide {
  align-items: center;
  display: flex;
  height: 180px;
}

.avatar {
  margin-right: 20px;
}

.avatar img {
  border-radius: 50px;
}

.username {
  font-weight: 800;
}
```
`app/assets/scripts/index.js`

```
$(document).ready(function(){
  var tweetsContainer = $('.tweets');

  tweetsContainer.slick({
    arrows: false,
    autoplay: true,
    autoplaySpeed: 2000,
    slidesToShow: 3,
    slidesToScroll: 1,
    vertical: true,
  });


  tweetsContainer.show();
});
```

Well, that's it. We finally have our scraper done. The final piece is: how do I run this?
You can run it in two ways:
1. `npm start` - this is going to execute the start command inside `scripts` section on `package.json`. This command is using nodemon. It's a tool that helps develop Node.js applications by automatically restarting the node application when file changes in the directory are detected. It's is ideal for the development environment.
2. `node app/server.js` - this is going to execute the server.js script directly. It's ideal for the production environment.
